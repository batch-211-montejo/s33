// console.log("Hello World")

// JAVASCRIPT SYNCHRONOUS AND ASYNCHRONOUS

/*
SYNCHRONOUS
	-Synchronous codes run in sequence. Thos means that each operation must wait for the previous ont to comple before executing

ASYNCHRONOUS
	-asynchrounous code runs in parallel. This means that an operation can occue while another one is still being processed.
	-Asynchronous code execution is often preferrable in situations where execution can be blocked. Some examples of this are network requests, long-running cal culations, file system operations, etx. Using asynchronous code in the browser ensure he page remains response and the user experience is mostly unaffected.
*/

// Example of Synchronous:
	console.log("Hello World")
	// cosnole.log("Hello Again") // will stop where the error begins

	// for (let i=0; i<=10000; i++){
	// 	console.log(i);
	// }

	// console.log("Hello it's me")


/*
	-API stands for Application Programming Interface
	-An application programming interface is a particular set of codes that allows software programs to communicate with each other
	-An API is the interface through which you access someone else's code or through which someone else's code accesses yours

	-Example:
		Google APIs
			https://developers.google.com/identity/sign-in/web/sign-in
		Youtube API
			https://developers.google.com/youtube//iframe_api_reference
*/

// FETCH API
	// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

/*
	-a "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

	A promise is in one of the theese three states:
	Pending:
		-Initial state, neither fulfilled nor rejected
	Fulfilled:
		-Operation was sucessfully completed
	Rejected:
		-Operation failed
*/

/*
	-By using the .then method, we can now check for the sttaus of the promise
	-The "fetch" method will return a "promise" that resolves to be a "response" object
	-The ".then" method catures the "response" object and returns another "promise" whch will evetually be "resolved" or "rejected"
	-Syntax:
		fetch('URL')
		.then(response => {})
*/

	// fetch('https://jsonplaceholder.typicode.com/posts')
	// .then(response => console.log(response.status)) //200 means ok

	// fetch('https://jsonplaceholder.typicode.com/posts')
	// // Use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
	// .then((response) => response.json())
	// // Print the converted JSON value from the "fetch" request
	// // Using multiple ".then" method to create a promise chain
	// .then((json) => console.log(json))


/*
	-The "async" and "await" keywords is another approach that can be used to achieve asynchronous codes
	-Used in functions to indicate which portions of the code should be waited
	-Creates an synchronous function
*/

	// async function fecthData(){
	// 	// waits for the "fetch" method to complete then restores the value in the "result" variable
	// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	// 	// Result returned fetch is returned as a promise
	// 	console.log(result)
	// 	// The returned "response" is an object
	// 	console.log(typeof result);

	// 	// We cannot access the content of the "response" byt directly accessing it's body property
	// 	console.log(result.body)
	// 	let json = await result.json();
	// 	console.log(json)
	// }
	// fetchData();



// GETTING A SPECIFIC POST
// Retrieves a specific post following the Rest API (retrieve, /posts/id:, GET)

	// fetch('https://jsonplaceholder.typicode.com/posts/1')
	// .then((response) => response.json())
	// .then((json) => console.log(json))

	/*
		Postman
			url: https://jsonplaceholder.typicode.com/posts/1
			method: GET
	*/



/*
	CREATING POST:
	Syntax:
		fetch('URL', options)
		.then((response) => {})
		.then((response) => {})
*/
		// Create a new post following the Rest API (create, /post/:id, )

		// fetch ('https://jsonplaceholder.typicode.com/posts', {
		// 	method: 'POST',
		// 	headers: {
		// 		'Content-Type': 'application/json'
		// 	},
		// 	body: JSON.stringify({
		// 		title: 'New post',
		// 		body: 'Hello World',
		// 		userId: 1
		// 	})
		// })
		// .then((response) => response.json())
		// .then((json) => console.log(json))

/*
	POSTMAN:
	url: https://jsonplaceholder.typicode.com/posts
	method: POST
	body: raw + JSON
		{
			"title": "My First Blog Post",
			"body": "Hello world!",
			"userId": 1
		}

*/

// UPDATING A POST
	// updates a specific post following the REST API (update, /posts/:id, PUT)
	// fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	// 	method: 'PUT',
	// 	headers: {
	// 		'Content-Type': 'application/json'
	// 	},
	// 	body: JSON.stringify({
	// 		id: 1,
	// 		title: 'Updated post',
	// 		body: 'Hello again!',
	// 		userId: 1
	// 	})
	// })
	// .then((response) => response.json())
	// .then((json) => console.log(json))

/*
POSTMAN:
	url:https://jsonplaceholder.typicode.com/posts/1
	method: PUT
	body: raw + json
	{
		"title": "My First Revised blog Post",
		"body": "Hello there! I revised this a bit.",
		"userId": 1
	}
*/

// Update a post using the PATCH method
/*
	-Updates a specific post following the Rest API (update, /posts/:id, Patch)
	-The differences between PUT and PATCH os the number of properties being changed
	-PATCH is used to update the whoel object
	-PUT is used to update a single.several properties

*/
	// fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	// 	method: 'PATCH',
	// 	headers: {
	// 		'Content-Type': 'application/json'
	// 	},
	// 	body: JSON.stringify({
	// 		title: 'Corrected post',
	// 	})
	// })
	// .then((response) => response.json())
	// .then((json) => console.log(json))


/*
POSTMAN:
	url:https://jsonplaceholder.typicode.com/posts/1
	method: PATCH
	body: raw + json
	{
		"title": "This is my final title"
	}

*/

// DELETING POST
// Deleting a specific post folllowing the Rest API (delete, /posts/:id, DELETE)
	// fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	// 	method: 'DELETE',
		
	// })
	// .then((response) => response.json())
	// .then((json) => console.log(json))

/*
POSTMAN:
	url:https://jsonplaceholder.typicode.com/posts/1
	method: DELETE
*/

// FILTERING THE POST
/*
	-The data can be filterd by sending the userId along with the URL
	-Information sent via the URL can be done by adding the question mark symbol (?)
	-Syntax:
		individual Parameters:
			'url?parameterName=value'
		Multiple Parameters:
			'url?ParamA=valueA&paramB=valueB'
*/
	
	// fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
	// .then((response) => response.json())
	// .then((json) => console.log(json))

	// fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
	// .then((response) => response.json())
	// .then((json) => console.log(json))

// RETRIEVE COMMENTS F A SPECIFIC POST
// Retrieving comments for a specific post following the Rest API (retrieve, /posts/:id, GET)
	fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then((response) => response.json())
	.then((json) => console.log(json))
