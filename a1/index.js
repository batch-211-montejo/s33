// console.log("Hello World")

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))


// Getting all to do list item
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {
	let list = json.map ((todo => {
		return todo.title;
	}))
	console.log(list)
})


// Getting a specific to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// Create a todo list item using POST Method
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify ({
		title: 'Created to Do List Item',
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Upadte a todo list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'To update the my to do list with a different data structure',
		status: "Pending",
		dateCompleted: 'Pending',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


// Updating a todo list item using PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
			status: "complete",
			dateCompleted: "07/09/21"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

// Delete
fetch ('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'DELETE'		
	})
	.then((response) => response.json())
	.then((json) => console.log(json))